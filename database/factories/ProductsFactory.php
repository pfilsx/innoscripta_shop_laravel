<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$titles = [
    'Margherita',
    'Capricciosa',
    'Quattro Formaggi',
    'Bolognese',
    'Mexicana',
    'Peperoni',
    'Napolitana',
    'Hawaii',
    'Marinara',
    'Frutti di Mare',
    'Crudo',
];

$factory->define(Product::class, function (Faker $faker) use ($titles) {
    return [
        'title' => $faker->unique()->randomElement($titles),
        'description' => $faker->text(120),
        'image_url' => 'img/pizza.jpg',
        'price' => random_int(300, 800)
    ];
});
