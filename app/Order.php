<?php

namespace App;

use App\Helpers\Cart;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{
    protected $fillable = [
        'name',
        'phone',
        'address',
        'total_price',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class);
    }

    public static function createFromRequestAndCart(array $requestData)
    {
        $order = new self($requestData);
        $order->user()->associate(Auth::user());
        $order->total_price = implode(' / ', Cart::getInstance()->getSignedTotalPrices());
        $order->save();
        $order->orderProducts()->saveMany(Cart::getInstance()->getOrderProducts());
        return $order;
    }
}
