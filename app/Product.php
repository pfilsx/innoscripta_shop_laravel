<?php

namespace App;

use App\Helpers\CurrencyConverter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\View;

/**
 * App\Product
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $image_url
 * @property int $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereImageUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Product extends Model
{

    protected $fillable = [
        'title',
        'description',
        'price',
        'image_url',
    ];

    protected $casts = [
        'price' => 'integer',
    ];

    public function getPrices(int $count = 1)
    {
        return CurrencyConverter::convert($this->price * $count);
    }

    public function getSignedPrices(int $count = 1)
    {
        $prices = $this->getPrices($count);
        array_walk($prices, function(&$price, $currency) {
            $price = $price.CurrencyConverter::CURRENCY_SIGNS[$currency];
        });
        return $prices;
    }


    public function getView()
    {
        $prices = $this->getPrices();
        return View::make('blocks.product', [
            'product' => $this,
            'prices' => $prices
        ]);
    }
}
