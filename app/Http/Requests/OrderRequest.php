<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:255',
            'phone' => ['required', 'regex:/^\+7\(\d{3}\)\d{3}\-\d{2}-\d{2}$/i'],
            'address' => 'required|min:3|max:500',
        ];
    }
}
