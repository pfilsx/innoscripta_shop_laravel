<?php

namespace App\Http\Controllers;

use App\Helpers\Cart;
use App\Http\Requests\OrderRequest;
use App\Order;
use App\Product;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = Product::paginate(10);
        return view('home', [
            'products' => $products
        ]);
    }

    public function checkoutForm()
    {
        if (Cart::getInstance()->isEmpty()){
            return redirect()->route('home');
        }
        return view('checkout');
    }

    public function checkoutProceed(OrderRequest $request)
    {
        if (Cart::getInstance()->isEmpty()){
            return redirect()->route('home');
        }
        // TODO handle exceptions
        Order::createFromRequestAndCart($request->validated());
        Cart::getInstance()->clear();
        $request->session()->flash('message', 'Order successfully created!');
        return redirect()->route('home');
    }
}
