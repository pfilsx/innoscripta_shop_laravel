<?php


namespace App\Http\Controllers;


use App\Order;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function orderHistory()
    {
        $orders = Order::where(['user_id' => Auth::id()])->get();
        return view('order_history', [
            'orders' => $orders
        ]);
    }
}
