<?php


namespace App\Http\Controllers;


use App\Helpers\Cart;
use App\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {
        $cart = Cart::getInstance();
        return $cart->getView();
    }

    public function clearCart()
    {
        Cart::getInstance()->clear();
        return redirect()->route('home');
    }

    public function addToCart(Request $request)
    {
        if ($request->wantsJson()) {
            $product = Product::where('id', $request->get('product_id'))->first();
            if ($product) {
                $cart = Cart::getInstance();
                $cart->addProduct($product);
                return response()->json([
                    'success' => true,
                    'result' => [
                        'products_count' => $cart->count()
                    ]
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'result' => [
                        'message' => trans('app.product_not_found')
                    ]
                ]);
            }
        }
        return redirect()->route('home');
    }

    public function removeFromCart(Request $request)
    {
        if ($request->wantsJson()) {
            $product = Product::where('id', $request->get('product_id'))->first();
            $cart = Cart::getInstance();
            if ($product) {
                $cart->removeProduct($product, $request->get('full_remove', false));
            }
            return response()->json([
                'success' => true,
                'result' => [
                    'products_count' => $cart->count()
                ]
            ]);
        }
        return redirect()->route('home');
    }
}
