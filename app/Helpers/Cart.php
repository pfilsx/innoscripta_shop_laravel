<?php


namespace App\Helpers;


use App\OrderProduct;
use App\Product;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;


class Cart
{
    const DELIVERY_COST = 100;

    private static $instance;

    private $products;
    private $deliveryPrices;
    private $productModels;

    private function __construct()
    {
        $this->products = Session::get('cart', []);
    }

    /**
     * Saves current cart in session
     */
    public function save()
    {
        Session::put('cart', $this->products);
    }

    /**
     * Clear current cart and save it
     */
    public function clear()
    {
        $this->products = [];
        $this->save();
    }

    /**
     * Checks if current cart is empty
     * @return bool
     */
    public function isEmpty()
    {
        return empty($this->products);
    }

    /**
     * Returns count of products: total or by product id
     * @param null $id
     * @return int
     */
    public function count($id = null)
    {
        $totalCount = 0;
        foreach ($this->products as $productId => $productAmount) {
            if ($id === null || $productId == $id) {
                $totalCount += $productAmount;
            }
        }
        return $totalCount;
    }

    /**
     * Adds product in current cart and saves cart.
     * If product with same id already exists in cart increments its count
     * @param Product $product
     */
    // TODO required functionality to limit the quantity of products to improve business logic and avoid possible overflow
    public function addProduct(Product $product)
    {
        if (!array_key_exists($product->id, $this->products)) {
            $this->products[$product->id] = 1;
        } else {
            $this->products[$product->id]++;
        }
        $this->save();
    }

    /**
     * Removes product(fully or one instance) from current cart and saves cart.
     * If product with same id not in cart ignores it.
     * If after removing of the instance we have less than 1 instance of product in cart removes product fully.
     * @param Product $product
     * @param bool $fullRemove - forces fully remove of product from cart
     */
    public function removeProduct(Product $product, bool $fullRemove = false)
    {
        if (array_key_exists($product->id, $this->products)) {
            $fullRemove
                ? $this->products[$product->id] = 0
                : $this->products[$product->id]--;
            if ($this->products[$product->id] < 1) {
                unset($this->products[$product->id]);
            }
        }
        $this->save();
    }

    /**
     * Returns total prices of products in current cart in array format(eg ['us' => 35, 'eu' => 37]).
     * @return array
     */
    public function getTotalPrices()
    {
        $totalPrices = $this->getDeliveryPrices();
        $products = $this->getProductModels();
        foreach ($products as $product) {
            $count = $this->products[$product->id];
            $prices = $product->getPrices($count);
            foreach ($prices as $key => $price) {
                $totalPrices[$key] += $price;
            }
        }
        return $totalPrices;
    }

    /**
     * Returns total prices with their signs of products in current cart in array format(eg ['us' => '35$', 'eu' => '37€']).
     * @return array
     */
    public function getSignedTotalPrices()
    {
        $prices = $this->getTotalPrices();
        array_walk($prices, function (&$price, $currency) {
            $price = $price . CurrencyConverter::CURRENCY_SIGNS[$currency];
        });
        return $prices;
    }

    /**
     * Returns array of OrderProduct models for current cart.
     * @return array
     */
    public function getOrderProducts()
    {
        $result = [];
        $products = $this->getProductModels();
        foreach ($products as $product) {
            $count = $this->products[$product->id];
            $orderProduct = new OrderProduct([
                'count' => $count,
                'price' => implode(' / ', $product->getSignedPrices($count))
            ]);
            $orderProduct->product()->associate($product);
            $result[] = $orderProduct;
        }
        return $result;
    }

    /**
     * Returns cart view.
     * @return \Illuminate\Contracts\View\View
     */
    public function getView()
    {
        $viewProducts = $this->getOrderProducts();
        return View::make('cart', [
            'products' => $viewProducts,
            'totalPrices' => implode(' / ', $this->getSignedTotalPrices()),
            'deliveryPrices' => $this->getDeliveryPrices()
        ]);
    }

    public static function getInstance()
    {
        return self::$instance ?? (self::$instance = new self());
    }

    private function getDeliveryPrices()
    {
        return $this->deliveryPrices ?? ($this->deliveryPrices = CurrencyConverter::convert(self::DELIVERY_COST));
    }

    private function getProductModels()
    {
        return $this->productModels ?? ($this->productModels = Product::whereIn('id', array_keys($this->products))->get());
    }
}
