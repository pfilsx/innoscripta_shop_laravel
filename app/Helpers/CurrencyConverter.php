<?php


namespace App\Helpers;


class CurrencyConverter
{
    const CURRENCY_RATES = [
        'us' => 61.26,
        'eu' => 68.05,
    ];

    const CURRENCY_SIGNS = [
        'us' => '$',
        'eu' => '€'
    ];

    /**
     * Converts $amount of currency in all presented in CurrencyConverter::CURRENCY_RATES currencies.
     * @param int $amount
     * @return array
     */
    public static function convert(int $amount)
    {
        $result = [];
        foreach (self::CURRENCY_RATES as $currency => $rate) {
            $result[$currency] = number_format($amount / $rate, 2);
        }
        return $result;
    }
}
