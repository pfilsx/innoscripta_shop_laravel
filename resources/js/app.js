require('./bootstrap');
let inputmask = require('inputmask');
let preloader = $('.preloader');

$(document).ready(function () {
    preloader.fadeOut(750, function () {
        $(this).css('background', 'rgba(0,0,0, .5)'); // lets use preloader as ajax loader later
    });
    window.onbeforeunload = function () {
        preloader.fadeIn();
    };
    inputmask('+7(999)999-99-99').mask('input#phone');
});

$(document).ajaxSend(function () {
    preloader.fadeIn();
});
$(document).ajaxComplete(function () {
    preloader.fadeOut();
});
$(document).ajaxError(function (event, request) {
    preloader.fadeOut();
    alert('Something wrong with your request');
    console.log([event, request]);
});

$('.btn-to-cart').on('click', function (e) {
    e.preventDefault();
    addProductToCart(this);
});
$('body').on('click', '.count_control_link' ,function (e) {
    e.preventDefault();
    if ($(this).hasClass('count_control_remove')) {
        removeProductFromCart(this, 0, reloadCartView);
    } else if ($(this).hasClass('count_control_add')) {
        addProductToCart(this, reloadCartView);
    }
});
$('body').on('click', '.delete_control_link', function(e){
    e.preventDefault();
    removeProductFromCart(this, 1, reloadCartView);
});

function reloadCartView() {
    let cart = $('.cart');
    $.get({
        url: cart.attr('data-reload-url'),
        success: function (response) {
            let newCart = $(response).find('.cart');
            cart.html(newCart.html());
        }
    });
}

function addProductToCart(source, callback) {
    cartRequest($(source).attr('href'), {product_id: $(source).attr('data-id')}, callback);
}

function removeProductFromCart(source, full, callback) {
    cartRequest($(source).attr('href'), {
        product_id: $(source).attr('data-id'),
        full_remove: full
    }, callback);
}

function cartRequest(url, data, callback){
    $.post({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Accept': 'application/json'
        },
        url: url,
        data: data,
        dataType: 'json',
        success: function(response){
            if (response.success) {
                $('.cart-item-counter').text(response.result.products_count);
                if (typeof callback === 'function') {
                    callback(response.result);
                }
            } else {
                if (response.result && response.result.message) {
                    alert(response.result.message);
                }
            }
        }
    });
}
