@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>Order History</h1>
            </div>
        </div>
        <div class="row justify-content-center align-items-start">
            <div class="col">
                <table class="table table-striped text-left">
                    @foreach($orders as $order)
                        <tr>
                            <td>{{ $order->created_at }}</td>
                            <td>{{ $order->address }}</td>
                            <td>
                                @foreach($order->orderProducts as $orderProduct)
                                    {{$orderProduct->product->title}} x{{ $orderProduct->count }}<br>
                                @endforeach
                            </td>
                            <td class="text-right">
                                {!!  $order->total_price !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
