@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <h1>Pizza menu</h1>
        </div>
    </div>
    <div class="row d-flex justify-content-start align-items-stretch">
        @foreach ($products as $product)
            <div class="col-12 col-md-4 col-lg-3 mb-4">
                {!! $product->getView() !!}
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col">
            {{ $products->links() }}
        </div>
    </div>
</div>
@endsection
