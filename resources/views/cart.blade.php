@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>Cart</h1>
            </div>
        </div>
        <div class="row justify-content-center align-items-start">
            <div class="col">
                <div class="cart" data-reload-url="{{ route('cart') }}">
                    @if(!empty($products))
                        <table class="table table-hover cart-table">
                            @foreach($products as $orderProduct)
                                <tr>
                                    <td class="text-center w-auto">
                                        <img class="product-img" src="{{ asset($orderProduct->product->image_url) }}" alt="">
                                    </td>
                                    <td>
                                        <b>{{ $orderProduct->product->title }}</b>
                                    </td>
                                    <td>
                                        <div class="count_control d-flex align-items-center text-center">
                                            <a data-id="{{ $orderProduct->product->id }}" href="{{ route('remove_from_cart') }}"
                                               class="count_control_link count_control_remove">-</a>
                                            <span class="count_control_value">{{ $orderProduct->count }}</span>
                                            <a data-id="{{ $orderProduct->product->id }}" href="{{ route('add_to_cart') }}"
                                               class="count_control_link count_control_add">+</a>
                                        </div>
                                    </td>
                                    <td>
                                        {{ $orderProduct->price }}
                                    </td>
                                    <td class="text-center">
                                        <a data-id="{{ $orderProduct->product->id }}" href="{{ route('remove_from_cart') }}"
                                           class="delete_control_link">x</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="cart-total text-right">
                            <p class="mb-0">
                                Total:  {{ $totalPrices }}
                            </p>
                            <p class="small text-muted">
                                With delivery price included:
                                {{ $deliveryPrices['us'].\App\Helpers\CurrencyConverter::CURRENCY_SIGNS['us'] }}
                                /
                                {{ $deliveryPrices['eu'].\App\Helpers\CurrencyConverter::CURRENCY_SIGNS['eu'] }}</p>
                        </div>
                        <div class="cart-btns text-right">
                            <a href="{{ route('checkout_form') }}" class="btn cart-btn">Proceed to checkout</a>
                        </div>
                    @else
                        <div class="text-center">
                            <p>Your Order Cart is empty.</p>
                            <a href="{{ route('home') }}" class="btn btn-primary">Continue ordering...</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
