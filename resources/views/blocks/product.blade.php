<div class="card product-card">
    <img src="{{ asset($product->image_url) }}" class="card-img-top" alt="...">
    <div class="card-body">
        <h4 class="card-title">{{ $product->title }}</h4>
        <p class="card-text">{{ $product->description }}</p>
    </div>
    <div class="card-footer">
        <div class="row d-flex justify-content-center align-items-center">
            <div class="col-md-5">
                {{ $prices['us'].\App\Helpers\CurrencyConverter::CURRENCY_SIGNS['us']  }}
                <br>
                {{ $prices['eu'].\App\Helpers\CurrencyConverter::CURRENCY_SIGNS['eu'] }}
            </div>
            <div class="col-md-7 text-center">
                <a href="{{ route('add_to_cart') }}" data-id="{{ $product->id }}" class="btn btn-outline-primary btn-to-cart">Add to cart</a>
            </div>
        </div>
    </div>
</div>
