<?php

return [
    'product_not_found' => 'Selected product not found in our database.',
];
