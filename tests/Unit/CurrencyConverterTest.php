<?php


namespace Tests\Unit;


use App\Helpers\CurrencyConverter;
use Tests\TestCase;

class CurrencyConverterTest extends TestCase
{
    public function testConst()
    {
        $this->assertEquals(array_keys(CurrencyConverter::CURRENCY_RATES), array_keys(CurrencyConverter::CURRENCY_SIGNS));
    }

    public function testConvert()
    {
        $value = 100;
        $result = CurrencyConverter::convert($value);
        $this->assertIsArray($result);
        $this->assertCount(count(CurrencyConverter::CURRENCY_RATES), $result);
        foreach ($result as $value) {
            $this->assertIsNumeric($value);
        }
    }
}
