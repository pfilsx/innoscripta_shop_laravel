<?php


namespace Tests\Unit;


use App\Helpers\Cart;
use Tests\Models\Product;
use Tests\TestCase;

class CartTest extends TestCase
{
    public function testSingleton()
    {
        $cart = Cart::getInstance();
        $this->assertInstanceOf(Cart::class, $cart);
        $this->assertSame($cart, Cart::getInstance());
    }

    /**
     * @param array $products
     *
     * @dataProvider productsProvider
     */
    public function testAddProduct(array $products)
    {
        $cart = Cart::getInstance();
        $this->assertTrue($cart->isEmpty());
        $productsCount = [];
        foreach ($products as $product) {
            $cart->addProduct($product);
            array_key_exists($product->id, $productsCount)
                ? $productsCount[$product->id]++
                : $productsCount[$product->id] = 1;
        }
        $this->assertFalse($cart->isEmpty());
        $this->assertEquals(count($products), $cart->count());
        foreach ($productsCount as $id => $count) {
            $this->assertEquals($count, $cart->count($id));
        }
        $cart->clear();
        $this->assertTrue($cart->isEmpty());
    }

    /**
     * @param array $products
     *
     * @dataProvider productsProvider
     */
    public function testRemoveProduct(array $products)
    {
        $cart = Cart::getInstance();
        $this->assertTrue($cart->isEmpty());
        $productsCount = [];
        foreach ($products as $product) {
            $cart->addProduct($product);
            array_key_exists($product->id, $productsCount)
                ? $productsCount[$product->id]++
                : $productsCount[$product->id] = 1;
        }
        $removeProduct = $products[0];
        $cart->removeProduct($removeProduct);
        $this->assertEquals(--$productsCount[$removeProduct->id], $cart->count($removeProduct->id));
        $cart->removeProduct($removeProduct, true);
        $this->assertEquals(0, $cart->count($removeProduct->id));
        $cart->clear();
        $this->assertTrue($cart->isEmpty());
    }

    public function productsProvider()
    {
        yield [
            [
                new Product([
                    'id' => 1,
                    'title' => 'Test Product',
                    'description' => 'Test product',
                    'price' => 100,
                    'image_url' => 'test.png'
                ])
            ]
        ];
        yield [
            [
                new Product([
                    'id' => 1,
                    'title' => 'Test Product',
                    'description' => 'Test product',
                    'price' => 100,
                    'image_url' => 'test.png'
                ]),
                new Product([
                    'id' => 1,
                    'title' => 'Test Product',
                    'description' => 'Test product',
                    'price' => 100,
                    'image_url' => 'test.png'
                ])
            ]
        ];
        yield [
            [
                new Product([
                    'id' => 1,
                    'title' => 'Test Product',
                    'description' => 'Test product',
                    'price' => 100,
                    'image_url' => 'test.png'
                ]),
                new Product([
                    'id' => 1,
                    'title' => 'Test Product',
                    'description' => 'Test product',
                    'price' => 100,
                    'image_url' => 'test.png'
                ]),
                new Product([
                    'id' => 2,
                    'title' => 'Test Product',
                    'description' => 'Test product',
                    'price' => 100,
                    'image_url' => 'test.png'
                ])
            ]
        ];
    }
}
