<?php

namespace Tests\Feature;

use App\Product;
use App\User;
use Artisan;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResponsesTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate:refresh --seed');
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSeeInOrder(['product-card', 'btn-to-cart']);
    }

    public function testCart()
    {
        $response = $this->get('/cart');

        $response->assertStatus(200);
        $response->assertSeeText('Your Order Cart is empty');
    }

    // expected redirect if cart is empty
    public function testCheckout()
    {
        $response = $this->get('/checkout');
        $response->assertStatus(302);
        $response->assertRedirect('/');

        $response = $this->post('/checkout', ['name' => 'test', 'phone' => '+7(911)111-11-11', 'address' => 'test address']);
        $response->assertStatus(302);
        $response->assertRedirect('/');
    }

    public function testHistory()
    {
        $response = $this->get('/order-history');
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        $user = factory(User::class)->create();

        $response = $this->actingAs($user)
            ->get('/order-history');
        $response->assertStatus(200);
        $response->assertSee('<h1>Order History</h1>');
    }

    public function testAddToCart()
    {
        $product = factory(Product::class)->create();
        $response = $this->post('/add-to-cart', ['product_id' => $product->id], ['Accept' => 'application/json']);
        $response->assertExactJson([
            'success' => true,
            'result' => [
                'products_count' => 1
            ]
        ])->assertSessionHas('cart', [$product->id => 1]);
        $response = $this->post('/add-to-cart', ['product_id' => $product->id], ['Accept' => 'application/json']);
        $response->assertExactJson([
            'success' => true,
            'result' => [
                'products_count' => 2
            ]
        ])->assertSessionHas('cart', [$product->id => 2]);


        $response = $this->post('/add-to-cart', [], ['Accept' => 'application/json']);
        $response->assertExactJson([
            'success' => false,
            'result' => [
                'message' => 'Selected product not found in our database.'
            ]
        ])->assertSessionHas('cart', [$product->id => 2]);

        $response = $this->post('/add-to-cart', ['product_id' => $product->id]);
        $response->assertStatus(302);
        $response->assertRedirect('/');

        $this->get('/clear-cart');
    }

    public function testRemoveFromCart()
    {
        $product = factory(Product::class)->create();
        $this->post('/add-to-cart', ['product_id' => $product->id], ['Accept' => 'application/json']);
        $response = $this->post('/remove-from-cart', [], ['Accept' => 'application/json']);
        $response->assertExactJson([
            'success' => true,
            'result' => [
                'products_count' => 1
            ]
        ])->assertSessionHas('cart', [$product->id => 1]);

        $response = $this->post('/remove-from-cart', ['product_id' => $product->id], ['Accept' => 'application/json']);
        $response->assertExactJson([
            'success' => true,
            'result' => [
                'products_count' => 0
            ]
        ])->assertSessionHas('cart', []);
        // checks what we don't have -1 product count in cart after this
        $response = $this->post('/remove-from-cart', ['product_id' => $product->id], ['Accept' => 'application/json']);
        $response->assertExactJson([
            'success' => true,
            'result' => [
                'products_count' => 0
            ]
        ])->assertSessionHas('cart', []);
    }
}
