<?php


namespace Tests\Models;


use App\Product as BaseProduct;

class Product extends BaseProduct
{
    protected $fillable = [
        'id',
        'title',
        'description',
        'price',
        'image_url'
    ];
}
