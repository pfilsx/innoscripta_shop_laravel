
## Build

1. Create `.env` file from `.env.template`
2. `composer install`
3. `npm i`
4. `npm run ` your env(dev/prod)
5. if nginx - don't forget to configure server
6. `php artisan migrate:refresh --seed` 
