<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// yagni
Auth::routes([
    'register' => false,
    'reset' => false
]);

Route::get('/', 'HomeController@index')->name('home');
Route::get('/order-history', 'UserController@orderHistory')->name('order_history');

Route::get('/checkout', 'HomeController@checkoutForm')->name('checkout_form');
Route::post('/checkout', 'HomeController@checkoutProceed')->name('checkout_proceed');

Route::get('/cart', 'CartController@index')->name('cart');
Route::post('/add-to-cart', 'CartController@addToCart')->name('add_to_cart');
Route::post('/remove-from-cart', 'CartController@removeFromCart')->name('remove_from_cart');

if (App::environment() !== 'production'){
    Route::get('/clear-cart', 'CartController@clearCart')->name('clear_cart');
}
